'use strict'

var localVideo = document.getElementById('local_video');

var startButton = document.getElementById('startButton');
var callButton = document.getElementById('callButton');
var hangupButton = document.getElementById('hangupButton');

var pc;
var localStream;
var socket = io.connect();

var config = {
    'iceServers': [{
        'urls': 'stun:stun.l.google.com:19302'
    }]
};

const offerOptions = {
    offerToReceiveVideo: 1,
    offerToReceiveAudio: 1
};

callButton.disabled = true;
hangupButton.disabled = true;

startButton.addEventListener('click', startAction);
callButton.addEventListener('click', callAction);
hangupButton.addEventListener('click', hangupAction);

function startAction() {
    //获取视频流
    navigator.mediaDevices.getUserMedia({
        video: true,
        audio: true
    }).then(function (mediastream) {
        localStream = mediastream;
        localVideo.srcObject = mediastream;
        startButton.disabled = true;
    }).catch(function (e) {
        console.log(JSON.stringify(e));
    });

}

socket.on('create', function (room, id) {
    console.log('调用者创建聊天房间');
    console.log(room + id);
});

socket.on('call', function () {
    callButton.disabled = false;
});

socket.on('signal', function (message) {
    if (pc !== 'undefined') {
        //设置callee的offer
        pc.setRemoteDescription(new RTCSessionDescription(message));
        console.log('remote answer');
    }
});

socket.on('ice', function (message) {
    if (pc !== 'undefined') {
        //接收到一个从远端页面通过信号通道发来的新的 ICE 候选地址信息，
        //本机可以通过调用RTCPeerConnection.addIceCandidate() 来添加一个 ICE 代理。
        pc.addIceCandidate(new RTCIceCandidate(message));
        console.log('become candidate');
    }
});

socket.emit('create or join', 'room');

function callAction() {
    callButton.disabled = true;
    hangupButton.disabled = false;

    //通过配置信息新建RTCPeerConnection对象
    pc = new RTCPeerConnection(config);

    //将上一步获取到的本地的音视频流添加到RTCPeerConnection对象中
    localStream.getTracks().forEach(track => pc.addTrack(track, localStream));

    //RTCPeerConnection通过createOffer创建本地SDP
    pc.createOffer(offerOptions).then(function (offer) {
        // 创建成功后调用setLocalDescription设置本地offer(会话控制消息,网络配置,媒体功能)
        pc.setLocalDescription(offer);
        //设置成功后,将本地offer发送到远端的服务器
        //即交换信令
        socket.emit('signal', offer);
    });

    // 查询本地对外的接入端
    pc.addEventListener('icecandidate', function (event) {
        var iceCandidate = event.candidate;
        if (iceCandidate) {
            //发现新的接入端就通知ICE服务器
            socket.emit('ice', iceCandidate);
        }
    });
}

/*
- Caller通过配置信息后，创建RTCPeerConnection对象，将本地的音视频流并封装在MediaStream对象中，并添加在刚刚创建的RTCPeerConnection对象中
- Caller通过createOffer创建本地SDP，创建成功后回调调用setLocalDescription
设置本地despription，设置成功后调用回调通过http协议将本地的SDP发送到远程的房间服务器，
接着通过ICE服务器查询本机对外的接入端(Condidate),每发现新的condidate，ice服务器会通过异步的方式告知，
- 本地则会调用相应的回调并将结果通过websocket发送给信令服务器
*/

function hangupAction() {

    localStream.getTracks().forEach(track => track.stop());
    pc.close();
    pc = null;
    hangupButton.disabled = true;
    callButton.disabled = true;
    startButton.disabled = false;

}