'use strict'

var remoteVideo = document.getElementById('remote_video');

var socket = io.connect();

var config = {
    'iceServers': [{
        'urls': 'stun:stun.l.google.com:19302'
    }]
};

var pc;

socket.emit('create or join', 'room');

socket.on('join', function (room, id) {
    console.log('被邀请者加入房间');
});

socket.on('signal', function (message) {
    pc = new RTCPeerConnection(config);
    //caller发来的offer
    pc.setRemoteDescription(new RTCSessionDescription(message));
    pc.createAnswer().then(function (answer) {
        pc.setLocalDescription(answer);
        //将本地的offer发送给caller   
        socket.emit('signal', answer);
    });
    pc.addEventListener('icecandidate', function (event) {
        var iceCandidate = event.candidate;//caller发送candidate的话
        if (iceCandidate) {
            // 通信, 通知caller添加一个 ICE 代理。
            socket.emit('ice', iceCandidate);
        }
    });
    //设置视频流
    pc.addEventListener('addstream', function (event) {
        remoteVideo.srcObject = event.stream;
    });
});

socket.on('ice', function (message) {
    pc.addIceCandidate(new RTCIceCandidate(message));
});