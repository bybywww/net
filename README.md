# [项目](https://gitee.com/bybywww/net)

# 如何运行?

项目目录如下:

- video
  - `node index.js`
- webchat
  - 客户端
    - ``cd client``
    - ``npm install``
    - `npm run serve`
  - 服务器
    - `npm install`
    - `npm run dev`



## 功能

- [x] 注册+登录

- [x] 群聊

- [x] 加好友私聊

- [x] 发图片+表情😁

- [x] 改头像

- [x] 搜索好友

- [x] 热门好友推荐

- [x] 显示聊天室的人数

- [x] 简易视频聊天 

  - 单工, 因本地设备限制

  

## 本地环境设置

- MongoDB
  - [安装地址](https://www.mongodb.com/download-center/community)
    - 提示: 不要选 `install MonogDB compass`
  - [安装教程](https://www.runoob.com/mongodb/mongodb-window-install.html)
    - 注意安装教程中, **命令行下运行 MongoDB 服务器** 和 **配置 MongoDB 服务** 任选一个方式启动就可以。
  - 记得加上环境变量
- Node 8.5.0+ 和 Npm 5.3.0+
  - [安装教程](https://www.runoob.com/nodejs/nodejs-install-setup.html)
  - **配置npm下载镜像源**  
    - 运行以下命令 假设大家都在win10下开发
    - `npm config set registry https://registry.npm.taobao.org`
    - `npm config get registry`





# 安装中可能遇到的问题

- `webchat/client`目录下运行`npm run serve`

  

  > 问题:
  >
  > This dependency was not found:
  >
  > * axios in ./src/api/axios.js

  解决方案:

  `npm install --save axios`

- `webchat`下  `npm run dev`

  >'nodemon' 不是内部或外部命令，也不是可运行的程序
  >或批处理文件。

  解决方案: 安装 `nodemon`

  `npm install -g nodemon`  (这个是全局安装)
