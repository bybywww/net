const isLocal = window.location.href.indexOf('debug') !== -1 || false
  //客户端的socket连接
export default {
    server: (process.env.NODE_ENV === 'development' || isLocal) ? 'http://127.0.0.1:9090/' : 'http://127.0.0.1:9090/',
}