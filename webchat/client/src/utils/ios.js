export default function () {
  const u = navigator.userAgent;
  const isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); // ios终端
  const classList = 'input';
  [].forEach.call(document.querySelectorAll(classList), function(el) {
    el.addEventListener("blur", function(e) {
      if (isIOS) {
        blurAdjust();
      }
    });
  });

  function blurAdjust(e) {
    setTimeout(() => {
      if (document.activeElement.tagName === 'INPUT' || document.activeElement.tagName === 'TEXTAREA') {
        return;
      }
      let result = 'pc';
      if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) { 
        result = 'ios';
      } else if (/(Android)/i.test(navigator.userAgent)) {
        result = 'android';
      }

      if (result === 'ios') {
        document.activeElement.scrollIntoViewIfNeeded(true);
      }
    }, 100);
  }
}
