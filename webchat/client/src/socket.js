import io from 'socket.io-client';
import config from '../config/client';
//客户端socket连接
const socket = io.connect(config.server);

export default socket;
