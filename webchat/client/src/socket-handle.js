import env from '@utils/env';
import socket from './socket';
import store from './store';

export async function handleInit({
  name,
  id,
  src,
  roomList
}) {
    // 此处逻辑需要抽离复用
	// 用做加入不同的房间
  socket.emit('login', {name, id, ...env});
  ['room1', 'room2'].forEach(item => {
    const obj = {
      name,
      src,
      roomid: item,
    };
    socket.emit('room', obj);
	//将加入的房间信息发给服务器端
  })
  await store.dispatch('getRoomHistory', { selfId: id })
}